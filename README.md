# Prerequisite
1.-docker-compose

2.-postman

# Docker
Start project Command:  
`docker-compose up`  

# Postman
import the `Back End Test.postman_collection.json` file to your postman

# Swagger
To see swagger documentation in the browser go to `http://localhost:3000/docs`
