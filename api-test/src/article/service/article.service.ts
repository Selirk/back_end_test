import { HttpService } from '@nestjs/axios';
import { Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { map } from 'rxjs';
import { Logger } from 'winston';
import { GetArticlesFiltersDto } from '../dto/get-article-filter.dto';
import { ArticlePaginationDto } from '../dto/article.pagination.dto';
import { Article } from '../interfaces/article.interface';

@Injectable()
export class ArticleService {
  BASE_URL = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
  constructor(
    @Inject('winston') private readonly logger: Logger,
    @InjectModel('Article') private readonly articleModel: Model<Article>,
    private httpService: HttpService,
  ) {}

  async onModuleInit(): Promise<void> {
    await this.createAllArticles();
  }

  async getArticlesWithfilter(
    filterDto: GetArticlesFiltersDto,
    paginationDto: ArticlePaginationDto,
  ): Promise<Article[]> {
    const { author, title, _tags } = filterDto;
    const { page, limit } = paginationDto;
    const filters: Partial<Record<keyof Article, any>> = {};
    filters.status = true;

    if (author) {
      filters.author = author;
    }
    if (title) {
      filters.title = title;
    }
    if (_tags) {
      filters._tags = _tags;
    }
    const articles = await this.articleModel.find(filters);
    if (!articles.length) {
      this.logger.info('Does not exist articles with filter:', filters);
    }
    const startIndex = (page - 1) * limit;
    const endtIndex = page * limit;

    return articles.slice(startIndex, endtIndex);
  }

  async findAll(): Promise<Article[]> {
    const { hits } = await this.httpService
      .get(this.BASE_URL)
      .pipe(map((response) => response.data))
      .toPromise();
    return hits;
  }

  async getArticles(): Promise<Article[]> {
    const config = {
      _id: 1,
      title: 1,
      story_title: 1,
      created_at: 1,
      comment_text: 1,
      story_url: 1,
      url: 1,
      author: 1,
    };
    const articles = await this.articleModel
      .find(
        {
          status: true,
        },
        config,
      )
      .sort({ created_at: -1 });
    return articles;
  }

  @Cron('0 * * * *')
  async createAllArticles(): Promise<void> {
    try {
      const articles = await this.findAll();
      await this.articleModel.insertMany(articles, { ordered: false });
      this.logger.debug('Data Base has been updated');
    } catch (error) {
      if (error.code) this.logger.debug('Duplicates have been ignored');
    }
  }

  async deleteArticle(articleID: string): Promise<Article> {
    const context = { context: this.constructor.name, articleID };
    this.logger.info('Deleting article', context);
    const updatedStatusArticle = await this.articleModel.findOneAndUpdate(
      { _id: articleID, status: true },
      { status: false },
    );
    return updatedStatusArticle;
  }
}
