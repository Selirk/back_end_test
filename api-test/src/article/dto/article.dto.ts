import { ApiProperty } from '@nestjs/swagger';

export class ArticleDto {
  @ApiProperty() created_at: Date;
  @ApiProperty() title: string;
  @ApiProperty() url: string;
  @ApiProperty() author: string;
  @ApiProperty() points: number;
  @ApiProperty() story_text: string;
  @ApiProperty() comment_text: string;
  @ApiProperty() num_comments: number;
  @ApiProperty() story_id: number;
  @ApiProperty() story_title: string;
  @ApiProperty() story_url: string;
  @ApiProperty() parent_id: number;
  @ApiProperty() created_at_i: number;
  @ApiProperty() _tags: Array<string>;
  @ApiProperty() objectID: string;
  @ApiProperty() _highlightResult: object;
}
