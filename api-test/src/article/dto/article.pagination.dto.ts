export class ArticlePaginationDto {
    page: number;
    limit: number;
  }
  