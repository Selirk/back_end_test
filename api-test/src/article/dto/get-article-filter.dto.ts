export class GetArticlesFiltersDto {
  author: string;
  title: string;
  _tags: Array<string>;
}
