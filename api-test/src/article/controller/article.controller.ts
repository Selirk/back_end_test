import {
  Controller,
  Get,
  Delete,
  Res,
  Param,
  HttpStatus,
  NotFoundException,
  Query,
  Inject,
} from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { Logger } from 'winston';
import { GetArticlesFiltersDto } from '../dto/get-article-filter.dto';
import { ArticleDto } from '../dto/article.dto';
import { ArticleService } from '../service/article.service';
import { ArticlePaginationDto } from '../dto/article.pagination.dto';

@ApiTags('articles')
@Controller('article')
export class ArticleController {
  constructor(
    @Inject('winston') private readonly logger: Logger,
    private readonly articleService: ArticleService,
  ) {}

  @Get('/')
  @ApiOkResponse({ type: ArticleDto, isArray: false })
  @ApiOperation({
    description:
      'Obtains every article in the database in pages of 5 and find articles by filters',
    summary: 'Get every article in the database',
  })
  @ApiQuery({
    type: GetArticlesFiltersDto,
    name: 'Filters',
  })
  async getArticles(
    @Query() paginationDto: ArticlePaginationDto,
    @Query() filterDto: GetArticlesFiltersDto,
  ): Promise<ArticleDto> {
    const context = { context: this.constructor.name, filterDto };
    this.logger.info('Getting articles', context);

    let article;
    if (Object.keys(filterDto).length) {
      article = this.articleService.getArticlesWithfilter(
        filterDto,
        paginationDto,
      );
    } else {
      article = this.articleService.getArticles();
    }
    const result = await article;
    return result;
  }

  @Delete('/:articleID')
  @ApiOkResponse({ type: ArticleDto, isArray: false })
  @ApiOperation({
    description:
      'Deletes an article but it keeps it in the database so it cannot be loaded again',
    summary: 'Deletes an article changing its status',
  })
  @ApiParam({
    name: 'articleID',
    type: 'string',
    example: '62003913593c780cb4071e23',
  })
  @ApiNotFoundResponse({})
  async deleteArticle(@Res() res, @Param('articleID') articleID) {
    const context = { context: this.constructor.name, articleID };
    this.logger.info('Deleting article', context);
    const articleDeleted = await this.articleService.deleteArticle(articleID);
    if (!articleDeleted) throw new NotFoundException('Article does not exist');
    return res
      .status(HttpStatus.OK)
      .json({ message: 'Article Deleted', articleDeleted });
  }
}
