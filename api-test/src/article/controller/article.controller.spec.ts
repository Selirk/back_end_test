import { Test, TestingModule } from '@nestjs/testing';
import { WinstonModule } from 'nest-winston';
import { ArticleService } from '../service/article.service';
import { ArticleController } from './article.controller';

describe('ArticleController', () => {
  let controller: ArticleController;
  const articleService = { getArticles: jest.fn(), deteleArticle: jest.fn() };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticleController],
      providers: [{ provide: ArticleService, useValue: articleService }],
      imports: [WinstonModule.forRoot({ silent: true })],
    }).compile();
    controller = module.get<ArticleController>(ArticleController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('#getArticles', () => {
    it('It should get articles', () => {
      articleService.getArticles.mockImplementation(() => {
        return [];
      });
      const result = articleService.getArticles();
      expect(articleService.getArticles).toHaveBeenCalled();
      expect(result).toBeTruthy();
    });
  });

  describe('#deleteArticles', () => {
    it('It should call deleteArticle method with expected param', () => {
      const deleteArticleSpy = jest.spyOn(articleService, 'deteleArticle');
      const noteId = 'noteId';
      articleService.deteleArticle(noteId);
      expect(deleteArticleSpy).toHaveBeenCalledWith(noteId);
    });
  });
});
